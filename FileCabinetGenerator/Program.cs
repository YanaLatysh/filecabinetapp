﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using MyLib;

namespace FileCabinetGenerator
{
    public class Program
    {
        private static bool needUseCsv = true;

        public static void Main(string[] args)
        {  
            string path = null;
            string recordAmount = null;
            string startId = null;
            for (int i = 0; i < args.Length; i++)
            { 
                string arg = args[i];
                var stringArg = arg.Split('=', 2, StringSplitOptions.RemoveEmptyEntries);
                if (stringArg[0].ToLower(CultureInfo.CurrentCulture) == "--output-type")
                {
                    if (stringArg[1] == "csv")
                    {
                        needUseCsv = true;
                    }                    
                }
                else if (arg.ToLower(CultureInfo.CurrentCulture) == "-t" && args.Length > i + 1)
                {
                    if (args[i + 1].ToLower(CultureInfo.CurrentCulture) == "csv")
                    {
                        needUseCsv = true;
                    }
                }               
                else if (stringArg[0].ToLower(CultureInfo.CurrentCulture) == "--output")
                {
                    path = stringArg[1];
                }
                else if (arg.ToLower(CultureInfo.CurrentCulture) == "-o" && args.Length > i + 1)
                {
                    path = args[i + 1];
                }
                else if (stringArg[0].ToLower(CultureInfo.CurrentCulture) == "--records-amount")
                {
                    recordAmount = stringArg[1];
                }
                else if (arg.ToLower(CultureInfo.CurrentCulture) == "-a" && args.Length > i + 1)
                {
                    recordAmount = args[i + 1];
                }
                else if (stringArg[0].ToLower(CultureInfo.CurrentCulture) == "--start-id")
                {
                    startId = stringArg[1];
                }
                else if (arg.ToLower(CultureInfo.CurrentCulture) == "-i" && args.Length > i + 1)
                {
                    startId = arg;
                }
            }
            var list = Creat("5", "15");
            var array = list.ToArray();
            Export(array, "C:\\Users\\shoto\\Downloads\\baleba.csv");

            Console.WriteLine($"{recordAmount} records were written to {path}");
            Console.WriteLine($"needUseCsv - {needUseCsv}, path - {path}, recordAmount - {recordAmount}, startId - {startId}");
        }

        private static List<FileCabinetRecord> Creat(string startId, string recordAmount)
        {
            int amount = int.Parse(recordAmount);
            int start = int.Parse(startId);
            List<FileCabinetRecord> list = new List<FileCabinetRecord>();
            for (int i = start; i <= start + amount - 1; i++)
            {
                FileCabinetRecord record = RandomRecord();
                record.Id = i;
                list.Add(record);
            }

            return list;
        }

        private static FileCabinetRecord RandomRecord()
        {
            Random rand = new Random();
            FileCabinetRecord record = new FileCabinetRecord();
            record.FirstName = StringGenerate();
            record.LastName = StringGenerate();
            record.DateOfBirth = DateTimeGenerate();
            record.Age = (short)rand.Next(0, 101);
            record.AverageSalary = (decimal)rand.NextDouble();          
            record.InitializationLetter = CharGenerate();
            return record;
        }

        private static string StringGenerate()
        {
            char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            Random rand = new Random();
            string word = null;
            int letterCount = rand.Next(2, 61);
            for (int i = 0; i <= letterCount; i++)
            {
                int letterNum = rand.Next(0, letters.Length - 1);
                word += letters[letterNum];
            }

            return word.Substring(0, 1) + word.Substring(1).ToLower();
        }

        private static DateTime DateTimeGenerate()
        {
            Random rand = new Random();
            DateTime start = new DateTime(1950, 1, 1);
            int range = (DateTime.Now - start).Days;
            return start.AddDays(rand.Next(range));
        }

        private static char CharGenerate()
        {
            Random rand = new Random();
            char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            int letterNum = rand.Next(0, letters.Length - 1);
            return letters[letterNum];
        }

        private static void Export(FileCabinetRecord[] record, string path)
        {
            FileCabinetServiceSnapshot snapShot = new FileCabinetServiceSnapshot(record);
            if (needUseCsv is true)
            {
                StreamWriter stream = new StreamWriter(path, true);
                snapShot.SaveToCsv(stream);
                stream.Close();
            }
            else
            {
                StreamWriter stream = new StreamWriter(path, false);
                snapShot.SaveToXml(stream);
                stream.Close();
            }
        }
    }
}
