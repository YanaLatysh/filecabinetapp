﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using MyLib;

/// <summary>
/// Work with user record.
/// </summary>
public class FileCabinetMemoryService : IFileCabinetService
{
    private readonly IRecordValidator validator;
    private readonly Dictionary<string, List<FileCabinetRecord>> firstNameDictionary = new Dictionary<string, List<FileCabinetRecord>>();
    private readonly Dictionary<string, List<FileCabinetRecord>> lastNameDictionary = new Dictionary<string, List<FileCabinetRecord>>();
    private readonly Dictionary<string, List<FileCabinetRecord>> dateOfBirthDictionary = new Dictionary<string, List<FileCabinetRecord>>();
    private List<FileCabinetRecord> list = new List<FileCabinetRecord>();

    public FileCabinetMemoryService(IRecordValidator validator)
    {
        this.validator = validator;
    }

    /// <summary>
    /// Creat new record.
    /// </summary>
    /// <param name="rec"> Parameter object. </param>
    /// <returns> Return ID record. </returns>
    public int CreateRecord(FileCabinetRecord rec)
    {
        if (rec is null)
        {
            throw new ArgumentNullException(nameof(rec));
        }

        this.validator.ValidateParameters(rec);
        var record = new FileCabinetRecord
        {
            Id = this.list.Count + 1,
            FirstName = rec.FirstName,
            LastName = rec.LastName,
            DateOfBirth = rec.DateOfBirth,
            Age = rec.Age,
            AverageSalary = rec.AverageSalary,
            InitializationLetter = rec.InitializationLetter,
        };

        this.list.Add(record);
        DictionaryExt.AddExt(this.firstNameDictionary, record.FirstName, record);
        DictionaryExt.AddExt(this.lastNameDictionary, record.LastName, record);
        DictionaryExt.AddExt(this.dateOfBirthDictionary, record.DateOfBirth.ToString("yyyy-MMM-dd", CultureInfo.InvariantCulture), record);
        return record.Id;
    }

    /// <summary>.
    /// Get records.
    /// </summary>
    /// <returns> Return list of records. </returns>
    public ReadOnlyCollection<FileCabinetRecord> GetRecords()
    {
        var readOnlyList = new ReadOnlyCollection<FileCabinetRecord>(this.list);
        return readOnlyList;
    }

    /// <summary>
    /// Get count of records.
    /// </summary>
    /// <returns> Return count of records. </returns>
    public int GetStat()
    {
        return this.list.Count;
    }

    /// <summary>
    /// Edit record.
    /// </summary>
    /// <param name="rec"> Parameter object. </param>
    public void EditRecord(FileCabinetRecord rec)
    {
        if (rec is null)
        {
            throw new ArgumentNullException(nameof(rec));
        }

        this.validator.ValidateParameters(rec);

        if (this.list.Exists(x => x.Id == rec.Id))
        {
            int index = 0;
            for (int i = 0; i < this.list.Count; i++)
            {
                if (this.list[i].Id == rec.Id)
                {
                    index = i;
                }
            }

            FileCabinetRecord record = this.list[index];
            record.FirstName = rec.FirstName;
            record.LastName = rec.LastName;
            record.DateOfBirth = rec.DateOfBirth;
            record.Age = rec.Age;
            record.AverageSalary = rec.AverageSalary;
            record.InitializationLetter = rec.InitializationLetter;
            DictionaryExt.AddExt(this.firstNameDictionary, record.FirstName, record);
            DictionaryExt.AddExt(this.lastNameDictionary, record.LastName, record);
            DictionaryExt.AddExt(this.dateOfBirthDictionary, record.DateOfBirth.ToString("yyyy-MMM-dd", CultureInfo.InvariantCulture), record);
        }
        else
        {
            throw new ArgumentException("Id is not exist");
        }
    }

    /// <summary>
    /// Find records by first name in dictionary.
    /// </summary>
    /// <param name="firstName"> Search parameter by first name. </param>
    /// <returns> Return array of records with the searched first name. </returns>
    public ReadOnlyCollection<FileCabinetRecord> FindByFirstName(string firstName)
    {
        var dictFindByFirstName = new ReadOnlyCollection<FileCabinetRecord>(this.firstNameDictionary[firstName]);
        return dictFindByFirstName;
    }

    /// <summary>
    /// Find records by last name in dictionary.
    /// </summary>
    /// <param name="lastName"> Search by first name. </param>
    /// <returns> Return array of records with the searched last name. </returns>
    public ReadOnlyCollection<FileCabinetRecord> FindByLastName(string lastName)
    {
        var dictFindByLastName = new ReadOnlyCollection<FileCabinetRecord>(this.lastNameDictionary[lastName]);
        return dictFindByLastName;
    }

    /// <summary>
    /// Find records by date of birth in dictionary.
    /// </summary>
    /// <param name="dateOfBirth"> Search by date of birth. </param>
    /// <returns> Return array of records with the searched date of birth. </returns>
    public ReadOnlyCollection<FileCabinetRecord> FindByDateOfBirth(string dateOfBirth)
    {
        var dictFindByDateOfBirth = new ReadOnlyCollection<FileCabinetRecord>(this.dateOfBirthDictionary[dateOfBirth]);
        return dictFindByDateOfBirth;
    }

    /// <summary>
    /// Make snapshot.
    /// </summary>
    /// <returns> New instance of FileCabinetServiceSnapshot. </returns>
    public FileCabinetServiceSnapshot MakeSnapshot()
    {
        return new FileCabinetServiceSnapshot(this.list.ToArray());
    }

    /// <summary>
    /// Add snapshot to the list.
    /// </summary>
    /// <param name="snapshot"> FileCabinetServiceSnapshot class instance. </param>
    public void Restore(FileCabinetServiceSnapshot snapshot)
    {
        if (snapshot is null)
        {
            throw new ArgumentNullException(nameof(snapshot));
        }

        var records = snapshot.Records;
        List<FileCabinetRecord> list = new List<FileCabinetRecord>(records);

        for (int i = 0; i < list.Count; i++)
        {
            bool isEqual = false;
            for (int j = 0; j < this.list.Count; j++)
            {
                if (this.list[j] == list[i])
                {
                    isEqual = true;
                    this.list[j] = list[i];
                    break;
                }
            }

            if (isEqual == false)
            {
                this.list.Add(list[i]);
            }
        }
    }
}