﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;
using MyLib;

namespace FileCabinetApp
{
    /// <summary>
    /// Main classd.
    /// </summary>
    public static class Program
    {
        private const string DeveloperName = "Yana Latysh";
        private const string HintMessage = "Enter your command, or enter 'help' to get help.";
        private const int CommandHelpIndex = 0;
        private const int DescriptionHelpIndex = 1;
        private const int ExplanationHelpIndex = 2;

        private static bool isRunning = true;
        private static FileStream fileStream;
        private static IFileCabinetService fileCabinetService;
        private static bool needUseCustomService;

        private static Tuple<string, Action<string>>[] commands = new Tuple<string, Action<string>>[]
        {
            new Tuple<string, Action<string>>("help", PrintHelp),
            new Tuple<string, Action<string>>("exit", Exit),
            new Tuple<string, Action<string>>("stat", Stat),
            new Tuple<string, Action<string>>("creat", Creat),
            new Tuple<string, Action<string>>("list", List),
            new Tuple<string, Action<string>>("edit", Edit),
            new Tuple<string, Action<string>>("find", Find),
            new Tuple<string, Action<string>>("export", Export),
            new Tuple<string, Action<string>>("import", Import),
        };

        private static string[][] helpMessages = new string[][]
        {
            new string[] { "help", "prints the help screen", "The 'help' command prints the help screen." },
            new string[] { "exit", "exits the application", "The 'exit' command exits the application." },
            new string[] { "stat", "gets the statistics", "The 'stat' command gets the statistics." },
            new string[] { "creat", "creat user data", "The 'creat' command creats user data" },
            new string[] { "list", "return list of users", "The 'list' command returns list of users" },
            new string[] { "edit", "edit user data", "The 'edit' command edits user data" },
            new string[] { "find", "find user data", "The 'find' command finds user data" },
            new string[] { "export", "export user data", "The 'export' command exports user data" },
            new string[] { "import", "import user data", "The 'import' command imports user data" },
        };

        /// <summary>
        /// Main method.
        /// </summary>
        /// <param name="args"> Parameter. </param>
        public static void Main(string[] args)
        {
            if (args is null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            Console.WriteLine($"File Cabinet Application, developed by {Program.DeveloperName}");

            bool needUseFilesystemService = false; // Должно быть false, true чтобы проверять проще.
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];
                if (arg.ToLower(CultureInfo.CurrentCulture) == "--validation-rules=custom")
                {
                    needUseCustomService = true;
                }
                else if (arg.ToLower(CultureInfo.CurrentCulture) == "-v" && args.Length > i + 1)
                {
                    if (args[i + 1].ToLower(CultureInfo.CurrentCulture) == "custom")
                    {
                        needUseCustomService = true;
                    }
                }

                if (arg.ToLower(CultureInfo.CurrentCulture) == "--storage" || arg.ToLower(CultureInfo.CurrentCulture) == "-s")
                {
                    if (args[i + 1].ToLower(CultureInfo.CurrentCulture) == "file")
                    {
                        needUseFilesystemService = true;
                    }
                }
            }

            if (needUseFilesystemService)
            {
                Console.WriteLine("Using FilesystemService");
                fileStream = new FileStream("cabinet-records.db", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fileCabinetService = new FileCabinetFilesystemService(fileStream);
            }
            else if (needUseCustomService)
            {
                Console.WriteLine("Using custom validation rules");
                fileCabinetService = new FileCabinetMemoryService(new CustomValidator());
            }
            else
            {
                Console.WriteLine("Using default validation rules");
                fileCabinetService = new FileCabinetMemoryService(new DefaultValidator());
            }

            Console.WriteLine(Program.HintMessage);
            Console.WriteLine();

            do
            {
                Console.Write("> ");
                var inputs = Console.ReadLine().Split(' ', 2);
                const int commandIndex = 0;
                var command = inputs[commandIndex];

                if (string.IsNullOrEmpty(command))
                {
                    Console.WriteLine(Program.HintMessage);
                    continue;
                }

                var index = Array.FindIndex(commands, 0, commands.Length, i => i.Item1.Equals(command, StringComparison.InvariantCultureIgnoreCase));
                if (index >= 0)
                {
                    const int parametersIndex = 1;
                    var parameters = inputs.Length > 1 ? inputs[parametersIndex] : string.Empty;
                    commands[index].Item2(parameters);
                }
                else
                {
                    PrintMissedCommandInfo(command);
                }
            }
            while (isRunning);
        }

        /// <summary>
        /// Method for convertation and validation.
        /// </summary>
        /// <typeparam name="T"> Type of parameter value. </typeparam>
        /// <param name="converter"> Name of delegate converter. </param>
        /// <param name="validator"> Name of delegate validator. </param>
        /// <returns> Return results of convertation and validation. </returns>
        public static T ReadInput<T>(Func<string, Tuple<bool, string, T>> converter, Func<T, Tuple<bool, string>> validator)
        {
            if (converter is null || validator is null)
            {
                throw new ArgumentNullException(nameof(converter));
            }

            do
            {
                T value;

                var input = Console.ReadLine();
                var conversionResult = converter(input);

                if (!conversionResult.Item1)
                {
                    Console.WriteLine($"Conversion failed: {conversionResult.Item2}. Please, correct your input.");
                    continue;
                }

                value = conversionResult.Item3;

                var validationResult = validator(value);
                if (!validationResult.Item1)
                {
                    Console.WriteLine($"Validation failed: {validationResult.Item2}. Please, correct your input.");
                    continue;
                }

                return value;
            }
            while (true);
        }

        /// <summary>
        /// Converter for string.
        /// </summary>
        /// <param name="value"> Parameter string value. </param>
        /// <returns> Return string value. </returns>
        public static Tuple<bool, string, string> StringConverter(string value)
        {
            return new Tuple<bool, string, string>(true, null, value);
        }

        /// <summary>
        /// Converter for short.
        /// </summary>
        /// <param name="value"> Parameter string value. </param>
        /// <returns> Return short value. </returns>
        public static Tuple<bool, string, short> ShortConverter(string value)
        {
            try
            {
                var val = short.Parse(value, CultureInfo.InvariantCulture);
                return new Tuple<bool, string, short>(true, null, val);
            }
            catch (SystemException e)
            {
                return new Tuple<bool, string, short>(false, e.Message, 0);
            }
        }

        /// <summary>
        /// Converter for decimal.
        /// </summary>
        /// <param name="value"> Parameter string value. </param>
        /// <returns> Return decimal value. </returns>
        public static Tuple<bool, string, decimal> DecimalConverter(string value)
        {
            try
            {
                var val = decimal.Parse(value, CultureInfo.InvariantCulture);
                return new Tuple<bool, string, decimal>(true, null, val);
            }
            catch (ArgumentException e)
            {
                return new Tuple<bool, string, decimal>(false, e.Message, 0);
            }
        }

        /// <summary>
        /// Converter for char.
        /// </summary>
        /// <param name="value"> Parameter string value. </param>
        /// <returns> Return char value. </returns>
        public static Tuple<bool, string, char> CharConverter(string value)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var val = value.ToCharArray()[0];
            return new Tuple<bool, string, char>(true, null, val);
        }

        /// <summary>
        /// Converter for DateTime.
        /// </summary>
        /// <param name="value"> Parameter string value. </param>
        /// <returns> Return DateTime value. </returns>
        public static Tuple<bool, string, DateTime> DateConverter(string value)
        {
            try
            {
                DateTime.TryParseExact(value, "MM/dd/yy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime val);
                return new Tuple<bool, string, DateTime>(true, null, val);
            }
            catch (ArgumentException e)
            {
                return new Tuple<bool, string, DateTime>(false, e.Message, DateTime.Now);
            }
        }

        /// <summary>
        /// Validator for string value.
        /// </summary>
        /// <typeparam name="T"> Type of parameter value. </typeparam>
        /// <param name="value"> Parameter. </param>
        /// <returns> Validation result. </returns>
        public static Tuple<bool, string> StringValidator<T>(T value)
        {
            if (value is string stringValue)
            {
                if (needUseCustomService)
                {
                    if (stringValue.Length < 3 || stringValue.Length > 20 || string.IsNullOrWhiteSpace(stringValue))
                    {
                        return new Tuple<bool, string>(false, "false");
                    }
                    else
                    {
                        return new Tuple<bool, string>(true, null);
                    }
                }
                else
                {
                    if (stringValue.Length < 2 || stringValue.Length > 60 || string.IsNullOrWhiteSpace(stringValue))
                    {
                        return new Tuple<bool, string>(false, "false");
                    }
                    else
                    {
                        return new Tuple<bool, string>(true, null);
                    }
                }
            }

            return new Tuple<bool, string>(false, "Value is not DateTime  ");
        }

        /// <summary>
        /// Validator for DateTime value.
        /// </summary>
        /// <typeparam name="T"> Type of parameter value. </typeparam>
        /// <param name="value"> Parameter. </param>
        /// <returns> Validation result. </returns>
        public static Tuple<bool, string> DateOfBirthValidator<T>(T value)
        {
            if (value is DateTime dateValue)
            {
                DateTime minValueDate;
                if (needUseCustomService)
                {
                    minValueDate = new (1940, 1, 1);
                }
                else
                {
                    minValueDate = new (1950, 1, 1);
                }

                if (dateValue > DateTime.Now || dateValue < minValueDate)
                {
                    return new Tuple<bool, string>(false, "false");
                }
                else
                {
                    return new Tuple<bool, string>(true, null);
                }
            }

            return new Tuple<bool, string>(false, "Value is not DateTime  ");
        }

        /// <summary>
        /// Validator for short value.
        /// </summary>
        /// <typeparam name="T"> Type of parameter value. </typeparam>
        /// <param name="value"> Parameter. </param>
        /// <returns> Validation result. </returns>
        public static Tuple<bool, string> ShortValidator<T>(T value)
        {
            if (value is short shortValue)
            {
                if (needUseCustomService)
                {
                    if (shortValue < 0 || shortValue > 100 || shortValue == 0)
                    {
                        return new Tuple<bool, string>(false, "false");
                    }
                    else
                    {
                        return new Tuple<bool, string>(true, null);
                    }
                }
                else
                {
                    if (shortValue < 0 || shortValue > 100)
                    {
                        return new Tuple<bool, string>(false, "false");
                    }
                    else
                    {
                        return new Tuple<bool, string>(true, null);
                    }
                }
            }

            return new Tuple<bool, string>(false, "Value is not DateTime  ");
        }

        /// <summary>
        /// Validator for decimal value.
        /// </summary>
        /// <typeparam name="T"> Type of parameter value. </typeparam>
        /// <param name="value"> Parameter. </param>
        /// <returns> Validation result. </returns>
        public static Tuple<bool, string> DecimalValidator<T>(T value)
        {
            if (value is decimal decimalValue)
            {
                if (decimalValue < 0)
                {
                    return new Tuple<bool, string>(false, "false");
                }
                else
                {
                    return new Tuple<bool, string>(true, null);
                }
            }

            return new Tuple<bool, string>(false, "Value is not DateTime  ");
        }

        /// <summary>
        /// Validator for char value.
        /// </summary>
        /// <typeparam name="T"> Type of parameter value. </typeparam>
        /// <param name="value"> Parameter. </param>
        /// <returns> Validation result. </returns>
        public static Tuple<bool, string> CharValidator<T>(T value)
        {
            if (value is char charValue)
            {
                if (char.IsWhiteSpace(charValue))
                {
                    return new Tuple<bool, string>(false, "false");
                }
                else
                {
                    return new Tuple<bool, string>(true, null);
                }
            }

            return new Tuple<bool, string>(false, "Value is not DateTime  ");
        }

        private static void PrintMissedCommandInfo(string command)
        {
            Console.WriteLine($"There is no '{command}' command.");
            Console.WriteLine();
        }

        private static void PrintHelp(string parameters)
        {
            if (!string.IsNullOrEmpty(parameters))
            {
                var index = Array.FindIndex(helpMessages, 0, helpMessages.Length, i => string.Equals(i[Program.CommandHelpIndex], parameters, StringComparison.InvariantCultureIgnoreCase));
                if (index >= 0)
                {
                    Console.WriteLine(helpMessages[index][Program.ExplanationHelpIndex]);
                }
                else
                {
                    Console.WriteLine($"There is no explanation for '{parameters}' command.");
                }
            }
            else
            {
                Console.WriteLine("Available commands:");

                foreach (var helpMessage in helpMessages)
                {
                    Console.WriteLine("\t{0}\t- {1}", helpMessage[Program.CommandHelpIndex], helpMessage[Program.DescriptionHelpIndex]);
                }
            }

            Console.WriteLine();
        }

        private static void Exit(string parameters)
        {
            fileStream.Close();
            Console.WriteLine("Exiting an application...");
            isRunning = false;
        }

        private static void Stat(string parameters)
        {
            var recordsCount = fileCabinetService.GetStat();
            Console.WriteLine($"{recordsCount} record(s).");
        }

        private static void Creat(string parameters)
        {
            FileCabinetRecord fileCabinetRecord = new FileCabinetRecord();
            fileCabinetRecord.Id = 33;

            Console.Write("First name: ");
            Func<string, Tuple<bool, string, string>> stringConverter = StringConverter;
            Func<string, Tuple<bool, string>> firstNameValidator = StringValidator;
            var firstName = ReadInput(stringConverter, firstNameValidator);
            fileCabinetRecord.FirstName = firstName;

            Console.Write("Last name: ");
            Func<string, Tuple<bool, string>> lastNameValidator = StringValidator;
            var lastName = ReadInput(stringConverter, lastNameValidator);
            fileCabinetRecord.LastName = lastName;

            Console.Write("Date of birth in format MM/dd/yy: ");
            Func<string, Tuple<bool, string, DateTime>> dateConverter = DateConverter;
            Func<DateTime, Tuple<bool, string>> dateOfBirthValidator = DateOfBirthValidator;
            var birth = ReadInput(dateConverter, dateOfBirthValidator);
            fileCabinetRecord.DateOfBirth = birth;

            Console.Write("Age: ");
            Func<string, Tuple<bool, string, short>> ageConverter = ShortConverter;
            Func<short, Tuple<bool, string>> ageValidator = ShortValidator;
            var age = ReadInput(ageConverter, ageValidator);
            fileCabinetRecord.Age = age;

            Console.Write("Average salary in $: ");
            Func<string, Tuple<bool, string, decimal>> decimalConverter = DecimalConverter;
            Func<decimal, Tuple<bool, string>> decimalValidator = DecimalValidator;
            var averageSalary = ReadInput(decimalConverter, decimalValidator);
            fileCabinetRecord.AverageSalary = averageSalary;

            Console.Write("Initialization letter: ");
            Func<string, Tuple<bool, string, char>> charConverter = CharConverter;
            Func<char, Tuple<bool, string>> charValidator = CharValidator;
            var letter = ReadInput(charConverter, charValidator);
            fileCabinetRecord.InitializationLetter = letter;

            try
            {
                var id = fileCabinetService.CreateRecord(fileCabinetRecord);
                Console.WriteLine($"Record #{id} is created.");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void List(string parameters)
        {
            var array = fileCabinetService.GetRecords();
            for (int i = 0; i < array.Count; i++)
            {
                var date = array[i].DateOfBirth.ToString("yyyy-MMM-dd", CultureInfo.InvariantCulture);
                Console.WriteLine($"#{array[i].Id}, {array[i].FirstName}, {array[i].LastName}, {date}, {array[i].Age}, {array[i].AverageSalary}, {array[i].InitializationLetter}");
            }
        }

        private static void Edit(string parameters)
        {
            FileCabinetRecord fileCabinetRecord = new FileCabinetRecord();
            var index = int.Parse(parameters, CultureInfo.InvariantCulture);
            var array = fileCabinetService.GetRecords();
            for (int i = 0; i < array.Count; i++)
            {
                if (array[i].Id == index)
                {
                    fileCabinetRecord.Id = index;
                    Console.Write("First name: ");
                    var firstname = Console.ReadLine();
                    fileCabinetRecord.FirstName = firstname;
                    Console.Write("Last name: ");
                    var lastName = Console.ReadLine();
                    fileCabinetRecord.LastName = lastName;
                    Console.Write("Date of birth in format MM/dd/yy: ");
                    DateTime.TryParseExact(Console.ReadLine(), "MM/dd/yy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime birth);
                    fileCabinetRecord.DateOfBirth = birth;
                    Console.Write("Age: ");
                    var age = short.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
                    fileCabinetRecord.Age = age;
                    Console.Write("Average salary in $: ");
                    var averageSalary = decimal.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
                    fileCabinetRecord.AverageSalary = averageSalary;
                    Console.Write("Initialization letter: ");
                    var letter = Console.ReadLine().ToCharArray();
                    fileCabinetRecord.InitializationLetter = letter[0];

                    try
                    {
                        fileCabinetService.EditRecord(fileCabinetRecord);
                        Console.WriteLine($"Record #{index} is updated.");
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    return;
                }
            }

            Console.WriteLine($"#{index} record is not found.");
        }

        private static void Find(string parameters)
        {
            var array = parameters.Split(' ', 2);
            var text = array[1].Trim('"');
            if (string.Equals(array[0], "firstname", StringComparison.InvariantCultureIgnoreCase))
            {
                var arr = fileCabinetService.FindByFirstName(text);
                for (int i = 0; i < arr.Count; i++)
                {
                    var date = arr[i].DateOfBirth.ToString("yyyy-MMM-dd", CultureInfo.InvariantCulture);
                    Console.WriteLine($"#{arr[i].Id}, {arr[i].FirstName}, {arr[i].LastName}, {date}");
                }
            }
            else if (string.Equals(array[0], "lastname", StringComparison.InvariantCultureIgnoreCase))
            {
                var arr = fileCabinetService.FindByLastName(text);
                for (int i = 0; i < arr.Count; i++)
                {
                    var date = arr[i].DateOfBirth.ToString("yyyy-MMM-dd", CultureInfo.InvariantCulture);
                    Console.WriteLine($"#{arr[i].Id}, {arr[i].FirstName}, {arr[i].LastName}, {date}");
                }
            }
            else if (string.Equals(array[0], "dateofbirth", StringComparison.InvariantCultureIgnoreCase))
            {
                var arr = fileCabinetService.FindByDateOfBirth(text);
                for (int i = 0; i < arr.Count; i++)
                {
                    var date = arr[i].DateOfBirth.ToString("yyyy-MMM-dd", CultureInfo.InvariantCulture);
                    Console.WriteLine($"#{arr[i].Id}, {arr[i].FirstName}, {arr[i].LastName}, {date}");
                }
            }
            else
            {
                Console.WriteLine("Неправильно введено свойство");
            }
        }

        private static void Export(string parameters)
        {
            var array = parameters.Split(' ', 2);
            string path = array[1];
            try
            {
                Console.Write($"File is exist - rewrite {path}? [Y/n] ");
                var text = Console.ReadLine();
                if (text.ToLower(CultureInfo.CurrentCulture) == "y")
                {
                    FileCabinetServiceSnapshot snapShot = fileCabinetService.MakeSnapshot();
                    if (array[0] == "csv")
                    {
                        StreamWriter stream = new StreamWriter(path, true);
                        snapShot.SaveToCsv(stream);
                        stream.Close();
                    }
                    else
                    {
                        StreamWriter stream = new StreamWriter(path, false);
                        snapShot.SaveToXml(stream);
                        stream.Close();
                    }

                    Console.WriteLine($"All records are exported to file {path}");
                }
                else
                {
                    Console.WriteLine($"You don't want to rewrite {path}");
                }
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine($"Export failed: can't open file {path}");
            }
        }

        private static void Import(string parameters)
        {
            var array = parameters.Split(' ', 2);
            string path = array[1];

            FileStream fileStream = new FileStream(path, FileMode.Open);
            StreamReader streamReader = new StreamReader(fileStream);
            FileCabinetServiceSnapshot snapshot = new FileCabinetServiceSnapshot();

            if (array[0] == "csv")
            {
                snapshot.LoadFromCsv(streamReader, needUseCustomService);
                fileCabinetService.Restore(snapshot);
                Console.WriteLine($"{snapshot.Records.Count} records were imported from {path}");
                streamReader.Close();
                fileStream.Close();
            }
            else
            {
                snapshot.LoadFromXml(streamReader);
                fileCabinetService.Restore(snapshot);
                Console.WriteLine($"{snapshot.Records.Count} records were imported from {path}");
                streamReader.Close();
                fileStream.Close();
            }
        }
    }
}