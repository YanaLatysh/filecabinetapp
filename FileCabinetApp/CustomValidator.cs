﻿using System;
using MyLib;

/// <summary>
/// Class custom validator.
/// </summary>
public class CustomValidator : IRecordValidator
{
    /// <summary>
    /// Method for parameter validation.
    /// </summary>
    /// <param name="rec"> Parameter object. </param>
    public void ValidateParameters(FileCabinetRecord rec)
    {
        if (rec is null)
        {
            throw new ArgumentNullException(nameof(rec));
        }

        if (string.IsNullOrWhiteSpace(rec.FirstName) || string.IsNullOrWhiteSpace(rec.LastName))
        {
            throw new ArgumentNullException(nameof(rec));
        }

        if (rec.FirstName.Length < 3 || rec.FirstName.Length > 20)
        {
            throw new ArgumentException("First name is less 3 or more 20 characters");
        }

        if (rec.LastName.Length < 3 || rec.LastName.Length > 30)
        {
            throw new ArgumentException("Last name is less 3 or more 30 characters");
        }

        DateTime minValueDate = new (1940, 1, 1);
        if (rec.DateOfBirth > DateTime.Now || rec.DateOfBirth < minValueDate)
        {
            throw new ArgumentException("Date of birth is not correct");
        }

        if (rec.Age < 0 || rec.Age > 100 || rec.Age == 0)
        {
            throw new ArgumentException("Age is less or equal than 0 or more than 100");
        }

        if (rec.AverageSalary < 0)
        {
            throw new ArgumentException("Average salary cannot be less than 0");
        }

        if (char.IsWhiteSpace(rec.InitializationLetter))
        {
            throw new ArgumentException("Initialization letter is white space");
        }
    }
}