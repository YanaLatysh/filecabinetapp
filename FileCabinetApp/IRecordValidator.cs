﻿using System;
using MyLib;

/// <summary>
/// Interface record validator.
/// </summary>
public interface IRecordValidator
{
    /// <summary>
    /// Method for parameter validation.
    /// </summary>
    /// <param name="rec"> Parameter object. </param>
    void ValidateParameters(FileCabinetRecord rec);
}