﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Text;
using MyLib;

namespace FileCabinetApp
{
    /// <summary>
    /// Class to export to binary file.
    /// </summary>
    public class FileCabinetFilesystemService : IFileCabinetService
    {
        private readonly FileStream fileStream;
        private readonly int recordSize = 278;
        private readonly int maxNameLength = 120;

        public FileCabinetFilesystemService(FileStream fileStream)
        {
            this.fileStream = fileStream;
        }

        /// <summary>
        /// Create record.
        /// </summary>
        /// <param name="rec"> FileCabinetRecord type parameter.  </param>
        /// <returns> Return Id. </returns>
        public int CreateRecord(FileCabinetRecord rec)
        {
            if (rec is null)
            {
                throw new ArgumentNullException(nameof(rec));
            }

            byte[] bytes = this.FileCabinetRecordToBytes(rec);
            this.fileStream.Position = this.fileStream.Length;
            this.fileStream.Write(bytes, 0, bytes.Length);
            this.fileStream.Flush();

            return rec.Id;
        }

        /// <summary>
        /// Edit record.
        /// </summary>
        /// <param name="rec"> FileCabinetRecord type parameter. </param>
        public void EditRecord(FileCabinetRecord rec)
        {
            if (rec is null)
            {
                throw new ArgumentNullException(nameof(rec));
            }

            this.fileStream.Position = 0;
            while (this.fileStream.Position < this.fileStream.Length)
            {
                byte[] recordBuffer = new byte[this.recordSize];
                this.fileStream.Read(recordBuffer, 0, this.recordSize);

                var record = this.BytesToFileCabinetRecord(recordBuffer);

                if (rec.Id == record.Id)
                {
                    record.FirstName = rec.FirstName;
                    record.LastName = rec.LastName;
                    record.DateOfBirth = rec.DateOfBirth;
                    record.Age = rec.Age;
                    record.AverageSalary = rec.AverageSalary;
                    record.InitializationLetter = rec.InitializationLetter;

                    byte[] bytes = this.FileCabinetRecordToBytes(record);
                    this.fileStream.Position -= this.recordSize;
                    this.fileStream.Write(bytes, 0, bytes.Length);
                    break;
                }
            }
        }

        /// <summary>
        /// Get list of records.
        /// </summary>
        /// <returns> Return ReadOnlyCollection type.</returns>
        public ReadOnlyCollection<FileCabinetRecord> GetRecords()
        {
            this.fileStream.Position = 0;
            List<FileCabinetRecord> list = new List<FileCabinetRecord>();
            while (this.fileStream.Position < this.fileStream.Length)
            {
                byte[] recordBuffer = new byte[this.recordSize];
                this.fileStream.Read(recordBuffer, 0, this.recordSize);

                var record = this.BytesToFileCabinetRecord(recordBuffer);
                list.Add(record);
            }

            var readOnlyList = new ReadOnlyCollection<FileCabinetRecord>(list);
            return readOnlyList;
        }

        /// <summary>
        /// Get count of records.
        /// </summary>
        /// <returns> Return int type of counts. </returns>
        public int GetStat()
        {
            return (int)this.fileStream.Length / this.recordSize;
        }

        /// <summary>
        /// Find record by first name.
        /// </summary>
        /// <param name="firstName"> String type parameter. </param>
        /// <returns> Return ReadOnlyCollection type list. </returns>
        public ReadOnlyCollection<FileCabinetRecord> FindByFirstName(string firstName)
        {
            this.fileStream.Position = 0;
            List<FileCabinetRecord> list = new List<FileCabinetRecord>();
            while (this.fileStream.Position < this.fileStream.Length)
            {
                byte[] recordBuffer = new byte[this.recordSize];
                this.fileStream.Read(recordBuffer, 0, this.recordSize);
                var record = this.BytesToFileCabinetRecord(recordBuffer);
                if (string.Equals(record.FirstName, firstName, StringComparison.CurrentCultureIgnoreCase))
                {
                    list.Add(record);
                }
            }

            var readOnlyList = new ReadOnlyCollection<FileCabinetRecord>(list);
            return readOnlyList;
        }

        /// <summary>
        /// Find record by last name.
        /// </summary>
        /// <param name="lastName"> String type parameter. </param>
        /// <returns> Return ReadOnlyCollection type list. </returns>
        public ReadOnlyCollection<FileCabinetRecord> FindByLastName(string lastName)
        {
            this.fileStream.Position = 0;
            List<FileCabinetRecord> list = new List<FileCabinetRecord>();
            while (this.fileStream.Position < this.fileStream.Length)
            {
                byte[] recordBuffer = new byte[this.recordSize];
                this.fileStream.Read(recordBuffer, 0, this.recordSize);
                var record = this.BytesToFileCabinetRecord(recordBuffer);
                if (string.Equals(record.LastName, lastName, StringComparison.CurrentCultureIgnoreCase))
                {
                    list.Add(record);
                }
            }

            var readOnlyList = new ReadOnlyCollection<FileCabinetRecord>(list);
            return readOnlyList;
        }

        /// <summary>
        /// Find record by date of birth name.
        /// </summary>
        /// <param name="dateOfBirth"> String type parameter. </param>
        /// <returns> Return ReadOnlyCollection type list. </returns>
        public ReadOnlyCollection<FileCabinetRecord> FindByDateOfBirth(string dateOfBirth)
        {
            this.fileStream.Position = 0;
            List<FileCabinetRecord> list = new List<FileCabinetRecord>();
            while (this.fileStream.Position < this.fileStream.Length)
            {
                byte[] recordBuffer = new byte[this.recordSize];
                this.fileStream.Read(recordBuffer, 0, this.recordSize);
                var record = this.BytesToFileCabinetRecord(recordBuffer);
                if (string.Equals(record.DateOfBirth.ToString("yyyy-MMM-dd", CultureInfo.InvariantCulture), dateOfBirth, StringComparison.CurrentCultureIgnoreCase))
                {
                    list.Add(record);
                }
            }

            var readOnlyList = new ReadOnlyCollection<FileCabinetRecord>(list);
            return readOnlyList;
        }

        /// <summary>
        /// Make snapshot.
        /// </summary>
        /// <returns> New instance of FileCabinetServiceSnapshot. </returns>
        public FileCabinetServiceSnapshot MakeSnapshot()
        {
            throw new NotImplementedException();
        }

        public void Restore(FileCabinetServiceSnapshot snapshot)
        {
            if (snapshot is null)
            {
                throw new ArgumentNullException(nameof(snapshot));
            }

            var recordsFromCsv = snapshot.Records;
            List<FileCabinetRecord> listFromCsv = new List<FileCabinetRecord>(recordsFromCsv);

            var recordsFromDb = this.GetRecords();
            List<FileCabinetRecord> listFromDb = new List<FileCabinetRecord>(recordsFromDb);

            for (int i = 0; i < listFromCsv.Count; i++)
            {
                bool isEqual = false;
                for (int j = 0; j < listFromDb.Count; j++)
                {
                    if (listFromCsv[i].Id == listFromDb[j].Id)
                    {
                        isEqual = true;
                        listFromDb[j] = listFromCsv[i];
                        break;
                    }
                }

                if (isEqual == false)
                {
                    this.AddToFileStream(listFromCsv[i]);
                }
            }
        }

        private byte[] FileCabinetRecordToBytes(FileCabinetRecord record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            var bytes = new byte[this.recordSize];
            using (var memoryStream = new MemoryStream(bytes))
            using (var binaryWriter = new BinaryWriter(memoryStream))
            {
                short status = 0;
                binaryWriter.Write(status);

                binaryWriter.Write(record.Id);

                byte[] firstNameBytes = Encoding.ASCII.GetBytes(record.FirstName);
                byte[] firstNameBuffer = new byte[this.maxNameLength];
                int firstNameLength = firstNameBytes.Length;
                if (firstNameLength > this.maxNameLength)
                {
                    firstNameLength = this.maxNameLength;
                }

                Array.Copy(firstNameBytes, 0, firstNameBuffer, 0, firstNameLength);

                binaryWriter.Write(firstNameBuffer);

                byte[] lastNameBytes = Encoding.ASCII.GetBytes(record.LastName);
                byte[] lastNameBuffer = new byte[this.maxNameLength];
                int lastNameLength = lastNameBytes.Length;
                if (lastNameLength > this.maxNameLength)
                {
                    lastNameLength = this.maxNameLength;
                }

                Array.Copy(lastNameBytes, 0, lastNameBuffer, 0, lastNameLength);

                binaryWriter.Write(lastNameBuffer);

                binaryWriter.Write(record.DateOfBirth.Year);

                binaryWriter.Write(record.DateOfBirth.Month);

                binaryWriter.Write(record.DateOfBirth.Day);

                binaryWriter.Write(record.Age);

                binaryWriter.Write(record.AverageSalary);

                binaryWriter.Write(record.InitializationLetter);
            }

            return bytes;
        }

        private FileCabinetRecord BytesToFileCabinetRecord(byte[] bytes)
        {
            if (bytes is null)
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            FileCabinetRecord record = new FileCabinetRecord();

            using (var memoryStream = new MemoryStream(bytes))
            using (var binaryReader = new BinaryReader(memoryStream))
            {
                short status = binaryReader.ReadInt16();
                record.Id = binaryReader.ReadInt32();

                byte[] firstNameBuffer = binaryReader.ReadBytes(this.maxNameLength);
                record.FirstName = Encoding.ASCII.GetString(firstNameBuffer, 0, this.maxNameLength);

                byte[] lastNameBuffer = binaryReader.ReadBytes(this.maxNameLength);
                record.LastName = Encoding.ASCII.GetString(lastNameBuffer, 0, this.maxNameLength);

                int year = binaryReader.ReadInt32();
                int month = binaryReader.ReadInt32();
                int day = binaryReader.ReadInt32();

                record.DateOfBirth = new DateTime(year, month, day);

                record.Age = binaryReader.ReadInt16();

                record.AverageSalary = binaryReader.ReadDecimal();

                record.InitializationLetter = binaryReader.ReadChar();

                return record;
            }
        }

        private void AddToFileStream(FileCabinetRecord record)
        {
            if (record is null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            byte[] bytes = this.FileCabinetRecordToBytes(record);
            this.fileStream.Position = this.fileStream.Length;
            this.fileStream.Write(bytes, 0, bytes.Length);
            this.fileStream.Flush();
        }
    }
}
