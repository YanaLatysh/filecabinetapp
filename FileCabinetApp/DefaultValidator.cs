﻿using System;
using MyLib;

/// <summary>
/// Class custom validator.
/// </summary>
public class DefaultValidator : IRecordValidator
{
    /// <summary>
    /// Method for parameter validation.
    /// </summary>
    /// <param name="rec"> Parameter object. </param>
    public void ValidateParameters(FileCabinetRecord rec)
    {
        if (rec is null)
        {
            throw new ArgumentNullException(nameof(rec));
        }

        if (string.IsNullOrWhiteSpace(rec.FirstName) || string.IsNullOrWhiteSpace(rec.LastName))
        {
            throw new ArgumentNullException(nameof(rec));
        }

        if (rec.FirstName.Length < 2 || rec.FirstName.Length > 60)
        {
            throw new ArgumentException("First name is less 2 or more 60 characters");
        }

        if (rec.LastName.Length < 2 || rec.LastName.Length > 60)
        {
            throw new ArgumentException("Last name is less 2 or more 60 characters");
        }

        DateTime minValueDate = new (1950, 1, 1);
        if (rec.DateOfBirth > DateTime.Now || rec.DateOfBirth < minValueDate)
        {
            throw new ArgumentException("Date of birth is not correct");
        }

        if (rec.Age < 0 || rec.Age > 100)
        {
            throw new ArgumentException("Age is less than 0 or more than 100");
        }

        if (rec.AverageSalary < 0)
        {
            throw new ArgumentException("Average salary cannot be less than 0");
        }

        if (char.IsWhiteSpace(rec.InitializationLetter))
        {
            throw new ArgumentException("Initialization letter is white space");
        }
    }
}