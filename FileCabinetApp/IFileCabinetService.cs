﻿using System;
using System.Collections.ObjectModel;
using MyLib;

/// <summary>
/// Interface IFileCabinetService.
/// </summary>
public interface IFileCabinetService
{
    /// <summary>
    /// Creat new record.
    /// </summary>
    /// <param name="rec"> Parameter object. </param>
    /// <returns> Return ID record. </returns>
    public int CreateRecord(FileCabinetRecord rec);

    /// <summary>
    /// Get records.
    /// </summary>
    /// <returns> Return list of records. </returns>
    public ReadOnlyCollection<FileCabinetRecord> GetRecords();

    /// <summary>
    /// Get count of records.
    /// </summary>
    /// <returns> Return count of records. </returns>
    public int GetStat();

    /// <summary>
    /// Edit record.
    /// </summary>
    /// <param name="rec"> Parameter object. </param>
    public void EditRecord(FileCabinetRecord rec);

    /// <summary>
    /// Find records by first name in dictionary.
    /// </summary>
    /// <param name="firstName"> Search parameter by first name. </param>
    /// <returns> Return array of records with the searched first name. </returns>
    public ReadOnlyCollection<FileCabinetRecord> FindByFirstName(string firstName);

    /// <summary>
    /// Find records by last name in dictionary.
    /// </summary>
    /// <param name="lastName"> Search by first name. </param>
    /// <returns> Return array of records with the searched last name. </returns>
    public ReadOnlyCollection<FileCabinetRecord> FindByLastName(string lastName);

    /// <summary>
    /// Find records by date of birth in dictionary.
    /// </summary>
    /// <param name="dateOfBirth"> Search by date of birth. </param>
    /// <returns>Return array of records with the searched date of birth. </returns>
    public ReadOnlyCollection<FileCabinetRecord> FindByDateOfBirth(string dateOfBirth);

    /// <summary>
    /// Make snapshot.
    /// </summary>
    /// <returns> New instance of FileCabinetServiceSnapshot. </returns>
    public FileCabinetServiceSnapshot MakeSnapshot();

    public void Restore(FileCabinetServiceSnapshot snapshot);
}
