﻿using System;
using System.Collections.Generic;
using MyLib;

/// <summary>
/// Actions with dictionary.
/// </summary>
public static class DictionaryExt
{
    /// <summary>
    /// Add record in dictionary.
    /// </summary>
    /// <param name="dictionary"> Dictionary. </param>
    /// <param name="key"> Dictionary key. </param>
    /// <param name="value"> Dictionary value. </param>
    public static void AddExt(Dictionary<string, List<FileCabinetRecord>> dictionary, string key, FileCabinetRecord value)
    {
        if (dictionary is null)
        {
            throw new ArgumentNullException(nameof(dictionary));
        }

        if (dictionary.ContainsKey(key))
        {
            List<FileCabinetRecord> list = dictionary[key];
            list.Add(value);
        }
        else
        {
            List<FileCabinetRecord> list = new List<FileCabinetRecord>();
            list.Add(value);
            dictionary.Add(key, list);
        }
    }
}