﻿using System;
using System.Globalization;
using System.IO;

namespace MyLib
{
    [Serializable]
    public class FileCabinetRecordCsvWriter
    {
        private TextWriter writer;

        public FileCabinetRecordCsvWriter(TextWriter writer)
        {
            this.writer = writer;
        }

        /// <summary>
        /// Method serializes the record.
        /// </summary>
        /// <param name="record"> Parameter record. </param>
        public void Write(FileCabinetRecord record)
        {
            if (record is null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            this.writer.Write(record.Id + ";");
            this.writer.Write(record.FirstName + ";");
            this.writer.Write(record.LastName + ";");
            this.writer.Write(record.DateOfBirth.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ";");
            this.writer.Write(record.Age + ";");
            this.writer.Write(record.AverageSalary + ";");
            this.writer.Write(record.InitializationLetter + ";");
            this.writer.WriteLine();
        }
    }
}
