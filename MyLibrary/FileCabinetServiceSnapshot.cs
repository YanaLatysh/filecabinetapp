﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;

namespace MyLib
{
    [Serializable]
    public class FileCabinetServiceSnapshot
    {
        private FileCabinetRecord[] records;

        public ReadOnlyCollection<FileCabinetRecord> Records 
        {
            get 
            {
                if (this.records == null) 
                {
                    return new ReadOnlyCollection<FileCabinetRecord>(Array.Empty<FileCabinetRecord>());
                }

                return new ReadOnlyCollection<FileCabinetRecord>(this.records);
            }
        }

        public FileCabinetServiceSnapshot(FileCabinetRecord[] records)
        {
            this.records = records;
        }

        public FileCabinetServiceSnapshot()
        {

        }

        /// <summary>
        /// Save to csv document.
        /// </summary>
        /// <param name="streamWriter"> Parameter. </param>
        public void SaveToCsv(StreamWriter streamWriter)
        {
            FileCabinetRecordCsvWriter textWriter = new FileCabinetRecordCsvWriter(streamWriter);
            for (int i = 0; i < this.records.Length; i++)
            {
                textWriter.Write(this.records[i]);
            }
        }

        /// <summary>
        /// Save to xml document.
        /// </summary>
        /// <param name="writer"> Parameter. </param>
        public void SaveToXml(StreamWriter writer)
        {
            //XmlWriterSettings settings = new XmlWriterSettings();
            //settings.Indent = true;
            //settings.IndentChars = "\t";
            //settings.NewLineOnAttributes = true;
            //XmlWriter xmlWriter = XmlWriter.Create(writer, settings);
            FileCabinetRecordXmlWriter fileCabinetRecordXmlWriter = new FileCabinetRecordXmlWriter(writer);
            fileCabinetRecordXmlWriter.Write(this.records);
            //for (int i = 0; i < this.records.Length; i++)
            //{
            //    fileCabinetRecordXmlWriter.Write(this.records[i]);
            //}

            writer.Close();
        }

        public void LoadFromCsv(StreamReader streamReader, bool kindOfValidation)
        {           
            FileCabinetRecordCsvReader reader = new FileCabinetRecordCsvReader(streamReader);
            var records = reader.ReadAll(kindOfValidation);
            FileCabinetRecord[] rec = new FileCabinetRecord[records.Count];
            records.CopyTo(rec, 0);
            this.records = rec;
        }

        public void LoadFromXml(StreamReader streamReader)
        {
            FileCabinetRecordXmlReader fileCabinetRecordXmlReader = new FileCabinetRecordXmlReader(streamReader);
            var records = fileCabinetRecordXmlReader.ReadAll();
            FileCabinetRecord[] rec = new FileCabinetRecord[records.Count];
            records.CopyTo(rec, 0);
            this.records = rec;
        }
    }
}


