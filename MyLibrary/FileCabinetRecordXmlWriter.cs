﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MyLib
{
    class FileCabinetRecordXmlWriter
    {
        private TextWriter writer;

        public FileCabinetRecordXmlWriter(TextWriter writer)
        {
            this.writer = writer;
        }

        /// <summary>
        /// Method serializes the record.
        /// </summary>
        /// <param name="record"> Parameter record. </param>
        public void Write(FileCabinetRecord[] records)
        {
            if (records is null)
            {
                throw new ArgumentNullException(nameof(records));
            }

            XmlSerializer serializer = new XmlSerializer(typeof(FileCabinetRecord[]), new XmlRootAttribute("RecordRoot"));
            serializer.Serialize(this.writer, records);
        }
    }
}
