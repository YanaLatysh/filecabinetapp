﻿using System;
using System.Xml.Serialization;

namespace MyLib
{
    [XmlType("record")]
    public class FileCabinetRecord
    {
        [XmlElement("Identif")]
        public int Id { get; set; }

        
        public string FirstName { get; set; }

        
        public string LastName { get; set; }

        
        public DateTime DateOfBirth { get; set; }

        
        public short Age { get; set; }

        
        public decimal AverageSalary { get; set; }

        public char InitializationLetter { get; set; }
    }
}

