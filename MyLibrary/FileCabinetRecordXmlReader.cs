﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace MyLib
{
    public class FileCabinetRecordXmlReader
    {
        private StreamReader reader;

        public FileCabinetRecordXmlReader(StreamReader reader)
        {
            this.reader = reader;
        }

        public IList<FileCabinetRecord> ReadAll()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(FileCabinetRecord[]), new XmlRootAttribute("RecordRoot"));
            FileCabinetRecord[] records = (FileCabinetRecord[])serializer.Deserialize(this.reader);
            return records;
        }
    }
}
