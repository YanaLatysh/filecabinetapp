﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace MyLib
{
    public class FileCabinetRecordCsvReader
    {
        private readonly StreamReader reader;

        public FileCabinetRecordCsvReader(StreamReader streamReader)
        {
            this.reader = streamReader;
        }

        public IList<FileCabinetRecord> ReadAll(bool kindOfValidation)
        {
            List<FileCabinetRecord> list = new List<FileCabinetRecord>();
            
            while (!reader.EndOfStream)
            {
                FileCabinetRecord record = new FileCabinetRecord();
                var line = reader.ReadLine();
                var values = line.Split(';');
                record.Id = int.Parse(values[0]);
                record.FirstName = values[1];
                record.LastName = values[2];               
                record.DateOfBirth = DateTime.ParseExact(values[3], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                record.Age = short.Parse(values[4]);
                record.AverageSalary = decimal.Parse(values[5]);
                var letter = values[6].ToCharArray();
                record.InitializationLetter = letter[0];

                var isCorrect = Validation(record, kindOfValidation);

                if (isCorrect is true)
                {
                    continue;
                }
                else
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (record.Id == list[i].Id)
                        {
                            list[i] = record;
                        }
                    }

                    list.Add(record);
                }
            }

            return list;
        }

        private bool Validation(FileCabinetRecord record, bool kindOfValidation)
        {
            DateTime minValueDate = new(1950, 1, 1);
            if (kindOfValidation is true)
            {
                if (string.IsNullOrWhiteSpace(record.FirstName) || string.IsNullOrWhiteSpace(record.LastName))
                {
                    return true;
                }
                else if (record.FirstName.Length < 3 || record.FirstName.Length > 20)
                {
                    return true;
                }
                else if (record.LastName.Length < 3 || record.LastName.Length > 30)
                {
                    return true;
                }
                else if (record.DateOfBirth > DateTime.Now || record.DateOfBirth < minValueDate)
                {
                    return true;
                }
                else if (record.Age < 0 || record.Age > 100 || record.Age == 0)
                {
                    return true;
                }
                else if (record.AverageSalary < 0)
                {
                    return true;
                }
                else  if (char.IsWhiteSpace(record.InitializationLetter))
                {
                    return true;
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(record.FirstName) || string.IsNullOrWhiteSpace(record.LastName))
                {
                    return true;
                }
                else if (record.FirstName.Length < 2 || record.FirstName.Length > 60)
                {
                    return true;
                }
                else if (record.LastName.Length < 2 || record.LastName.Length > 60)
                {
                    return true;
                }
                else if (record.DateOfBirth > DateTime.Now || record.DateOfBirth < minValueDate)
                {
                    return true;
                }
                else if (record.Age < 0 || record.Age > 100)
                {
                    return true;
                }
                else if (record.AverageSalary < 0)
                {
                    return true;
                }
                else if (char.IsWhiteSpace(record.InitializationLetter))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
